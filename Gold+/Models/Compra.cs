﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gold_.Models
{
    public class Compra
    {
        public string Cantidad { get; set; }
        public string Valor { get; set; }
        public DateTime FechaCompra { get; set; }
    }
}
