﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gold_.Models
{
    public class Densidad
    {
        public string ObjectId { get; set; }
        public DateTime CreatedAT { get; set; }
        public double PesoSeco { get; set; }
        public double PesoHumedo { get; set; }
        public double Ley { get; set; }
        public string UserId { get; set; }
    }
}
