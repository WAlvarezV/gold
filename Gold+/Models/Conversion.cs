﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gold_.Models
{
    public class Conversion
    {
        public string ObjectId { get; set; }
        public double Origen { get; set; }
        public double Cambio { get; set; }
        public double Cantidad { get; set; }
        public double Equivalente { get; set; }
    }
}
