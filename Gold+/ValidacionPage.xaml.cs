﻿using Parse;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Gold_
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ValidacionPage : Page
    {
        private string resourceName = "MyGoldApp";
        public ValidacionPage()
        {
            this.InitializeComponent();
            this.Loaded += ValidacionPage_Loaded;
                        
        }

        private void ValidacionPage_Loaded(object sender, RoutedEventArgs e)
        {
            Login();
        }

        private async void Login()
        {
            var loginCredential = GetCredentialFromLocker();
            try
            {
                
                if (loginCredential != null)
                {
                    loginCredential.RetrievePassword();
                    await ParseUser.LogInAsync(loginCredential.UserName, loginCredential.Password);
                    Frame.Navigate(typeof(MainPage));
                }
                else
                {
                    Frame rootFrame = Window.Current.Content as Frame;
                    rootFrame.Navigate(typeof(UserLoginPage));
                }
            }
            catch (Exception ex)
            {
                Frame.Navigate(typeof(UserLoginPage));
            }           
        }


        private Windows.Security.Credentials.PasswordCredential GetCredentialFromLocker()
        {
            Windows.Security.Credentials.PasswordCredential credential = null;
            try
            {
                var vault = new Windows.Security.Credentials.PasswordVault();
                var credentialList = vault.FindAllByResource(resourceName);
                if (credentialList.Count > 0)
                {
                    credential = credentialList[0];
                }
            }
            catch(Exception ex)
            {

            }            
            return credential;
        }


    }
}
