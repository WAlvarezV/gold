﻿using Parse;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Gold_
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ComprasPage : Page
    {
        string CurUserID;
        public ComprasPage()
        {
            this.InitializeComponent();
            var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
            var cantidad = loader.GetString("quantity");
            tbxCantidad.PlaceholderText = cantidad;
            var valor = loader.GetString("value");
            tbxValor.PlaceholderText = valor;
            var user = ParseUser.CurrentUser;
            CurUserID = user.ObjectId;
        }

        private void add_Click(object sender, RoutedEventArgs e)
        {
            if(tbxCantidad.Text == "")
            {
                SolidColorBrush scb1 = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
                tbxCantidad.BorderBrush = scb1;
            }
            else if(tbxValor.Text == "")
            {
                SolidColorBrush scb1 = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
                tbxValor.BorderBrush = scb1;
            }
            else
            {
                double cantidad = Double.Parse(tbxCantidad.Text.Replace('.', ','));
                double valor = Double.Parse(tbxValor.Text.Replace('.', ','));

                ParseObject parseobject = new ParseObject("Compras");
                parseobject.Add("cantidad", cantidad);
                parseobject.Add("valor", valor);
                parseobject.Add("userID", CurUserID);
                parseobject.SaveAsync();
            }

            

        }

        private void tbxCantidad_LostFocus(object sender, RoutedEventArgs e)
        {
            SolidColorBrush scb1;
            if (tbxCantidad.Text == "")
            {
                scb1 = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
                tbxCantidad.BorderBrush = scb1;
            }
            else
            {
                tbxCantidad.BorderBrush = (Brush)App.Current.Resources["SystemControlBackgroundAccentBrush"];
            }
        }

        private void tbxValor_LostFocus(object sender, RoutedEventArgs e)
        {
            SolidColorBrush scb1;
            if (tbxValor.Text == "")
            {
                scb1 = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
                tbxValor.BorderBrush = scb1;
            }
            else
            {
                tbxValor.BorderBrush = (Brush)App.Current.Resources["SystemControlBackgroundAccentBrush"];
            }
        }

        private void list_Click(object sender, RoutedEventArgs e)
        {
            Frame rootFrame = Window.Current.Content as Frame;
            rootFrame.Navigate(typeof(ListaComprasPage));
        }
    }
}
