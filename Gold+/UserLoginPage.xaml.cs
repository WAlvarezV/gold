﻿using Parse;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Gold_
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class UserLoginPage : Page
    {
        public UserLoginPage()
        {
            this.InitializeComponent();
            var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
            var userName = loader.GetString("user");
            tbxUserName.PlaceholderText = userName;            
            var password = loader.GetString("pass");
            tbxPassword.PlaceholderText = password;            
            
        }

        private async void txbLogin_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            try
            {
                if (tbxUserName.Text == "")
                {
                    SolidColorBrush scb1 = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
                    tbxUserName.BorderBrush = scb1;
                }
                else if (tbxPassword.Password == "")
                {
                    SolidColorBrush scb1 = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
                    tbxPassword.BorderBrush = scb1;
                }
                else
                {
                    await ParseUser.LogInAsync(tbxUserName.Text, tbxPassword.Password);
                    Frame.Navigate(typeof(MainPage));
                    if(cbxRecordarDatos.IsChecked == true)
                    {
                        var vault = new Windows.Security.Credentials.PasswordVault();
                        vault.Add(new Windows.Security.Credentials.PasswordCredential(
                            "MyGoldApp", tbxUserName.Text, tbxPassword.Password));
                        Frame.Navigate(typeof(MainPage));
                    }
                }
                
            }
            catch (Exception ex)
            {
                Frame.Navigate(typeof(UserLoginPage));
            }
            
        }

        private void txbCreateUser_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            Frame.Navigate(typeof(UserRegistroPage));
        }

        private void tbxResetPass_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            Frame.Navigate(typeof(RestablecerPassPage));
        }


        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Collapsed;
        }

        private void tbxUserName_LostFocus(object sender, RoutedEventArgs e)
        {
            SolidColorBrush scb1;
            if (tbxUserName.Text == "")
            {
                scb1 = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
                tbxUserName.BorderBrush = scb1;
            }
            else
            {
                tbxUserName.BorderBrush = (Brush)App.Current.Resources["SystemControlBackgroundAccentBrush"];
            }
        }

        private void tbxPassword_LostFocus(object sender, RoutedEventArgs e)
        {
            SolidColorBrush scb1;
            if (tbxPassword.Password == "")
            {
                scb1 = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
                tbxPassword.BorderBrush = scb1;
            }
            else
            {
                tbxPassword.BorderBrush = (Brush)App.Current.Resources["SystemControlBackgroundAccentBrush"];
            }
        }

        private void cbxRecordarDatos_Checked(object sender, RoutedEventArgs e)
        {
            if(tbxUserName.Text == "" || tbxPassword.Password =="")
            {
                tbxAlerta.Visibility = Visibility.Visible;
                cbxRecordarDatos.IsChecked = false;
            }
            else
            {
                tbxAlerta.Visibility = Visibility.Collapsed;
            }
        }
    }
}
