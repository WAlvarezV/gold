﻿using Gold_.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Gold_
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        string imgCharts = "http://goldprice.org/NewCharts/gold/images/gold_1d_o_USD.png";
        string densidad, configuracion, estadisticas, compras, convertir,salir;
        public MainPage()
        {
            this.InitializeComponent();
            imgChart.Source = new BitmapImage(new Uri(imgCharts));

        }

        private ObservableCollection<MenuItem> menuItems;

        

        private void ListBox_LostFocus(object sender, RoutedEventArgs e)
        {
            spvMenu.IsPaneOpen = false;
        }

        public ObservableCollection<MenuItem> MenuItems
        {
            get
            {
                if (menuItems == null)
                {
                    menuItems = new ObservableCollection<MenuItem>();

                    var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
                    densidad = loader.GetString("density");
                    configuracion = loader.GetString("setting");
                    estadisticas = loader.GetString("statistics");
                    compras = loader.GetString("shopping");
                    convertir = loader.GetString("convert");
                    salir = loader.GetString("logOut");

                    MenuItem item1 = new MenuItem() { Label = estadisticas, Icon = "World" };
                    MenuItem item2 = new MenuItem() { Label = densidad, Icon = "Calculator" };
                    MenuItem item3 = new MenuItem() { Label = convertir, Icon = "Switch" };
                    //MenuItem item4 = new MenuItem() { Label = compras, Icon = "Shop" };                                       
                    MenuItem item5 = new MenuItem() { Label = configuracion, Icon = "Setting" };
                    MenuItem item6 = new MenuItem() { Label = salir, Icon = "Contact" };

                    menuItems.Add(item1);
                    menuItems.Add(item2);
                    menuItems.Add(item3);
                    //menuItems.Add(item4);
                    menuItems.Add(item5);
                    menuItems.Add(item6);


                }

                return menuItems;
            }
            set { menuItems = value; }
        }

        private void btnHamburger_Click(object sender, RoutedEventArgs e)
        {
            spvMenu.IsPaneOpen = !spvMenu.IsPaneOpen;
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var itemSender = sender as ListBox;
            int value = itemSender.SelectedIndex;

            switch (value)
            {
                case 0:
                    Frame.Navigate(typeof(MainPage));
                    break;
                case 1:
                    txbHeader.Text = densidad;
                    contenido.Navigate(typeof(DensidadPage));
                    //spvMenu.IsPaneOpen = !spvMenu.IsPaneOpen;
                    break;
                case 2:
                    txbHeader.Text = convertir;
                    contenido.Navigate(typeof(ConvertirPage));
                    //spvMenu.IsPaneOpen = !spvMenu.IsPaneOpen;
                    break;
                //case 3:
                //    txbHeader.Text = compras;
                //    contenido.Navigate(typeof(ComprasPage));
                //    //spvMenu.IsPaneOpen = !spvMenu.IsPaneOpen;
                //    break;
                case 3:
                    txbHeader.Text = configuracion;
                    contenido.Navigate(typeof(ConfiguracionPage));
                    //spvMenu.IsPaneOpen = !spvMenu.IsPaneOpen;
                    break;
                case 4:
                    Frame.Navigate(typeof(UserLoginPage));
                    logOut();
                    break;
            }
        }
        
        private void cbx_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var txbCurrency = cbxCurrency.SelectedItem as TextBlock;
                var txbWeight = cbxWeight.SelectedItem as TextBlock;
                var txbTime = cbxTime.SelectedItem as TextBlock;

                string currency, weight, time;
                currency = txbCurrency.Text;
                weight = txbWeight.Tag.ToString();
                time = txbTime.Tag.ToString();


                imgCharts = "http://goldprice.org/NewCharts/gold/images/gold_" + time + "_" + weight + "_" + currency + ".png";
                imgChart.Source = new BitmapImage(new Uri(imgCharts));
            }
            catch(Exception ex)
            {
                
            }
        }

        private void logOut()
        {
            try
            {
                var loginCredential = GetCredentialFromLocker();
                var vault = new Windows.Security.Credentials.PasswordVault();
                vault.Remove(loginCredential);
            }
            catch(Exception ex)
            {

            }

            

        }

        private Windows.Security.Credentials.PasswordCredential GetCredentialFromLocker()
        {
            Windows.Security.Credentials.PasswordCredential credential = null;
            try
            {
                var vault = new Windows.Security.Credentials.PasswordVault();
                var credentialList = vault.FindAllByResource("MyGoldApp");
                if (credentialList.Count > 0)
                {
                    credential = credentialList[0];
                }
            }
            catch (Exception ex)
            {

            }
            return credential;
        }
    }
}
