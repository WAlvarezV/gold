﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gold_.Logica
{
    public class Calculadora
    {
        private double pesoS = 0;
        private double pesoH = 0;
        private double leyI = 0;
        private double leyF = 0;
        private double cantidad = 0;
        double resultado = 0;

        public Calculadora()
        {

        }

        public double PesoS
        {
            get { return pesoS; }
            set { pesoS = value; }
        }

        public double PesoH
        {
            get { return pesoH; }
            set { pesoH = value; }
        }

        public double LeyI
        {
            get { return leyI; }
            set { leyI = value; }
        }

        public double LeyF
        {
            get { return leyF; }
            set { leyF = value; }
        }

        public double Cantidad
        {
            get { return cantidad; }
            set { cantidad = value; }
        }

        public double Resultado
        {
            get { return resultado; }
            set { resultado = value; }
        }

        //METODO PARA CALCULAR LA CALIDAD DEL ORO DE ACUERDO A LOS PESOS
        public double CalcularLey()
        {
            resultado = Math.Round(((((pesoS - pesoH) * 23.03) / pesoS) - 2.1812), 3) * -1000;
            return resultado;
        }
        //METODO PARA CONVERTIR CANTIDAD DE ORO ENTRE DIFERENTES CALIDADES
        public double ConvertirLey()
        {
            resultado = Math.Round(((cantidad * leyI) / leyF), 1);
            return resultado;
        }


    }
}
