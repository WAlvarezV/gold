﻿using Gold_.Logica;
using Gold_.Models;
using Parse;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Gold_
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ConvertirPage : Page
    {
        Calculadora calc;
        public ConvertirPage()
        {
            this.InitializeComponent();
            calc = new Calculadora();
            loadConversiones();
            var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
            var cantidad = loader.GetString("quantity");
            tbxCantidad.PlaceholderText = cantidad;
            var leyDMina = loader.GetString("lawMine");
            tbxLey.PlaceholderText = leyDMina;
            

        }

        private void txbResutado_PointerReleased(object sender, PointerRoutedEventArgs e)
        {
            double origen, cambio, cantidad;

            if (tbxCantidad.Text == "")
            {
                SolidColorBrush scb1 = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
                tbxCantidad.BorderBrush = scb1;
            }
            else
            {
                var txbOrigen = cbxOrigen.SelectedItem as TextBlock;
                var txbcambio = cbxCambio.SelectedItem as TextBlock;
                cantidad = Double.Parse(tbxCantidad.Text.Replace('.', ','));
                if (txbOrigen.Text.Equals("Mina"))
                {
                    origen = Double.Parse(tbxLey.Text);
                    if (origen > 999 || origen < 100)
                    {
                        tbxAlerta.Visibility = Visibility.Visible;
                        txbResutado.Text = "Calcular";
                    }
                }
                else
                {
                    origen = Double.Parse(txbOrigen.Tag.ToString());
                }                
                cambio = Double.Parse(txbcambio.Tag.ToString());



                calc.Cantidad = cantidad;
                calc.LeyI = origen;
                calc.LeyF = cambio;

                calc.ConvertirLey();

               
                txbResutado.Text = "Equivalente: "+calc.Resultado;

                ParseObject parseobject = new ParseObject("Conversiones");
                parseobject.Add("cantidad", calc.Cantidad);
                parseobject.Add("leyOrigen", calc.LeyI);
                parseobject.Add("leyCambio", calc.LeyF);
                parseobject.Add("cantidadEquivalente", calc.Resultado);
                parseobject.SaveAsync();
            }
            
        }

        private void cbxOrigen_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var origen = cbxOrigen.SelectedItem as TextBlock;
            if(origen.Text.Equals("Mina"))
            {
                tbxLey.Visibility = Visibility.Visible;
            }
            else
            {
                tbxLey.Visibility = Visibility.Collapsed;
            }
        }

        public async void loadConversiones()
        {
            var query = from tabledata in ParseObject.GetQuery("Conversiones")
                        orderby tabledata.Get<DateTime>("createdAt") descending
                        select tabledata;
            query = query.Limit(3);

            var data = await query.FindAsync();
            foreach (ParseObject obj in data)
            {
                try
                {
                    Conversion c = new Conversion();
                    c.ObjectId = obj.ObjectId;
                    c.Origen = obj.Get<double>("leyOrigen");
                    c.Cambio = obj.Get<double>("leyCambio");
                    c.Cantidad = obj.Get<double>("cantidad");
                    c.Equivalente= obj.Get<double>("cantidadEquivalente");

                    lista.Add(c);
                }
                catch (Exception ex)
                {

                }



            }
        }

        //atributo
        private ObservableCollection<Conversion> lista;

        //propiedad
        public ObservableCollection<Conversion> Lista
        {
            get
            {
                if (lista == null)
                    lista = new ObservableCollection<Conversion>();
                return lista;
            }
            set { lista = value; }
        }

        private void tbxCantidad_LostFocus(object sender, RoutedEventArgs e)
        {
            SolidColorBrush scb1;
            if (tbxCantidad.Text == "")
            {
                scb1 = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
                tbxCantidad.BorderBrush = scb1;
            }
            else
            {
                tbxCantidad.BorderBrush = (Brush)App.Current.Resources["SystemControlBackgroundAccentBrush"];
            }
        }

        private void tbxLey_LostFocus(object sender, RoutedEventArgs e)
        {
            SolidColorBrush scb1;
            if (tbxLey.Text == "")
            {
                scb1 = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
                tbxLey.BorderBrush = scb1;
            }
            else
            {
                tbxLey.BorderBrush = (Brush)App.Current.Resources["SystemControlBackgroundAccentBrush"];
            }
        }

        private void tbx_TextChanged(object sender, TextChangedEventArgs e)
        {
            txbResutado.Text = "Calcular";
            tbxAlerta.Visibility = Visibility.Collapsed;
        }
    }
}
