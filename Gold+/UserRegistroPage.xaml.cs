﻿using Parse;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text.RegularExpressions;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Gold_
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class UserRegistroPage : Page
    {
        
        public UserRegistroPage()
        {
            this.InitializeComponent();
            var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
            var userName = loader.GetString("user");
            tbxUserName.PlaceholderText = userName;
            var email = loader.GetString("email");
            tbxMail.PlaceholderText = email;
            var password = loader.GetString("pass");
            tbxPassword.PlaceholderText = password;

            SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;
            SystemNavigationManager.GetForCurrentView().BackRequested += UserRegistroPage_BackRequested;
            
        }

        private void UserRegistroPage_BackRequested(object sender, BackRequestedEventArgs e)
        {
            Frame root = Window.Current.Content as Frame;

            if (root.CanGoBack && !e.Handled)
            {
                e.Handled = true;
                root.GoBack();

            }
        }

        private async void tbxUserName_LostFocus(object sender, RoutedEventArgs e)
        {
            var query = ParseUser.Query
                        .WhereEqualTo("username", tbxUserName.Text);
            var count = await query.CountAsync();
            if (count > 0)
            {
                tbxAlerta.Visibility = Visibility.Visible;
            }
            else
            {
                tbxAlerta.Visibility = Visibility.Collapsed;
            }
        }

        private async void txbRegistro_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            string username = tbxUserName.Text;
            string password = tbxPassword.Password;
            string email = tbxMail.Text;


            if (String.IsNullOrEmpty(username) || String.IsNullOrEmpty(password) || String.IsNullOrEmpty(email))
            {
                tbxAlertaDatos.Visibility = Visibility.Visible;
            }
            else
            {
                tbxAlertaDatos.Visibility = Visibility.Collapsed;
            }
            if (IsValidEmail(tbxMail.Text))
            {
                tbxAlertaMail.Visibility = Visibility.Collapsed;
                var user = new ParseUser()
                {
                    Username = tbxUserName.Text,
                    Password = tbxPassword.Password,
                    Email = tbxMail.Text
                };
                await user.SignUpAsync();
                var vault = new Windows.Security.Credentials.PasswordVault();
                vault.Add(new Windows.Security.Credentials.PasswordCredential(
                    "MyGoldApp", tbxUserName.Text, tbxPassword.Password));
                Frame.Navigate(typeof(MainPage));
            }
            else
            {
                tbxAlertaMail.Visibility = Visibility.Visible;
            }           
                       
        }

        

        public bool IsValidEmail(string email)
        {
            if (String.IsNullOrEmpty(email))
                return false;
            // Return true if strIn is in valid e-mail format.
            try
            {
                return Regex.IsMatch(email,
                      @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                      @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                      RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }
    }
}
