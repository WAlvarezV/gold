﻿using Gold_.Logica;
using Gold_.Models;
using Parse;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Gold_
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class DensidadPage : Page
    {
        Calculadora calc;
        string CurUserID;

        public DensidadPage()
        {
            this.InitializeComponent();
            
            calc = new Calculadora();
            var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
            var pesoSeco = loader.GetString("pesoSeco");
            tbxPesoSeco.PlaceholderText = pesoSeco;
            var pesoHumedo = loader.GetString("pesoHumedo");
            tbxPesoHumedo.PlaceholderText = pesoHumedo;
            var user = ParseUser.CurrentUser;
            CurUserID = user.ObjectId;
            

            loadDensidades();
        }

        

        private void txbResutado_PointerReleased(object sender, PointerRoutedEventArgs e)
        {
            try
            {
                double pesoSeco = Double.Parse(tbxPesoSeco.Text.Replace('.', ','));
                double pesoHumedo = Double.Parse(tbxPesoHumedo.Text.Replace('.', ','));

                if (pesoHumedo > pesoSeco || pesoSeco > 999.99 )
                {
                    tbxAlerta.Visibility = Visibility.Visible;
                }
                
                else
                {
                    
                    if (calc.PesoS != pesoSeco || calc.PesoH != pesoHumedo)
                    {
                        calc.PesoS = pesoSeco;
                        calc.PesoH = pesoHumedo;
                        calc.CalcularLey();
                        if(calc.Resultado > 999 || calc.Resultado < 100)
                        {
                            tbxAlerta.Visibility = Visibility.Visible;
                            txbResutado.Text = "Calcular";
                        }
                        else
                        {
                            tbxAlerta.Visibility = Visibility.Collapsed;
                            txbResutado.Text = "Ley " + calc.Resultado;

                            ParseObject parseobject = new ParseObject("Densidades");
                            parseobject.Add("pesoSeco", calc.PesoS);
                            parseobject.Add("pesoHumedo", calc.PesoH);
                            parseobject.Add("ley", calc.Resultado);
                            parseobject.Add("userID", CurUserID);


                            parseobject.SaveAsync();

                            Densidad d = new Densidad();
                            d.PesoSeco = calc.PesoS;
                            d.PesoHumedo = calc.PesoH;
                            d.Ley = calc.Resultado;
                            listSort(d);
                        } 
                    }   
                }              
            }
            catch (Exception ex)
            {

            }
            
        }

        public async void loadDensidades()
        {
            var query = from tabledata in ParseObject.GetQuery("Densidades")
                        where tabledata.Get<string>("userID").Equals(CurUserID)
                        orderby tabledata.Get<DateTime>("createdAt") descending
                        select tabledata;
            query = query.Limit(3);

            var data = await query.FindAsync();
            foreach (ParseObject obj in data)
            {
                try
                {
                    Densidad d = new Densidad();
                    d.ObjectId = obj.ObjectId;
                    d.PesoSeco = obj.Get<double>("pesoSeco");
                    d.PesoHumedo = obj.Get<double>("pesoHumedo");
                    d.Ley = obj.Get<double>("ley");

                    lista.Add(d);
                }
                catch (Exception ex)
                {

                }
            }
        }

        //atributo
        private ObservableCollection<Densidad> lista;

        //propiedad
        public ObservableCollection<Densidad> Lista
        {
            get
            {
                if (lista == null)
                    lista = new ObservableCollection<Densidad>();
                return lista;
            }
            set { lista = value; }
        }

        private void tbxPesoSeco_LostFocus(object sender, RoutedEventArgs e)
        {
            SolidColorBrush scb1;
            if (tbxPesoSeco.Text == "")
            {
                scb1 = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
                tbxPesoSeco.BorderBrush = scb1;
            }
            else
            {
                tbxPesoSeco.BorderBrush = (Brush)App.Current.Resources["SystemControlBackgroundAccentBrush"];
            }
        }

        private void tbxPesoHumedo_LostFocus(object sender, RoutedEventArgs e)
        {
            SolidColorBrush scb1;
            if (tbxPesoHumedo.Text == "")
            {
                scb1 = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
                tbxPesoHumedo.BorderBrush = scb1;
            }
            else
            {
                tbxPesoHumedo.BorderBrush = (Brush)App.Current.Resources["SystemControlBackgroundAccentBrush"];
            }
        }

        private void listSort(Densidad d)
        {
            

            switch (lista.Count<Densidad>())
            {
                case 0:
                    lista.Add(d);
                    break;
                case 1:
                    Densidad d1 = lista[0];
                    lista.Clear();
                    lista.Add(d);
                    lista.Add(d1);                    
                    break;
                case 2:                   
                    d1 = lista[0];
                    Densidad d2 = lista[1];
                    lista.Clear();
                    lista.Add(d);
                    lista.Add(d1);
                    lista.Add(d2);
                    break;
                case 3:
                    lista.RemoveAt(2);
                    d1 = lista[0];
                    d2 = lista[1];
                    lista.Clear();
                    lista.Add(d);
                    lista.Add(d1);
                    lista.Add(d2);
                    break;
            } 
        }

        private void tbx_TextChanged(object sender, TextChangedEventArgs e)
        {
            txbResutado.Text = "Calcular";
            tbxAlerta.Visibility = Visibility.Collapsed;
        }
    }
}
