﻿using Gold_.Models;
using Parse;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Gold_
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ListaComprasPage : Page
    {
        string CurUserID;
        public ListaComprasPage()
        {
            this.InitializeComponent();
            var user = ParseUser.CurrentUser;
            CurUserID = user.ObjectId;
            loadCompras();
        }

        public async void loadCompras()
        {

            var query = from tabledata in ParseObject.GetQuery("Compras")
                        where tabledata.Get<string>("userID").Equals(CurUserID)
                        orderby tabledata.Get<DateTime>("createdAt") descending
                        select tabledata;        
            
            var data = await query.FindAsync();
            foreach (ParseObject obj in data)
            {
                Compra c = new Compra();
                c.FechaCompra = (DateTime)obj.CreatedAt;
                c.Cantidad= ""+obj.Get<double>("cantidad");
                c.Valor = ""+obj.Get<double>("valor");
                

                lista.Add(c);
            }
        }



        //atributo
        private ObservableCollection<Compra> lista;

        //propiedad
        public ObservableCollection<Compra> Lista
        {
            get
            {
                if (lista == null)
                    lista = new ObservableCollection<Compra>();
                return lista;
            }
            set { lista = value; }
        }


    }
}
