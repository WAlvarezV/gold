﻿using Parse;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Gold_
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class RestablecerPassPage : Page
    {
        public RestablecerPassPage()
        {
            this.InitializeComponent();
            var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
            var email = loader.GetString("email");
            tbxMail.PlaceholderText = email;

            SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;
            SystemNavigationManager.GetForCurrentView().BackRequested += RestablecerPassPage_BackRequested;
        }

        private async void btnReset_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                await ParseUser.RequestPasswordResetAsync(tbxMail.Text);
                var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
                var resetPSmessage = loader.GetString("resetPSmessage");
                tbxAlerta.Text = resetPSmessage;
                tbxAlerta.Visibility = Visibility.Visible;
            }
            catch (Exception ex)
            {
                var loader = new Windows.ApplicationModel.Resources.ResourceLoader();
                var invalid = loader.GetString("invalid");
                tbxAlerta.Text = invalid;
                tbxAlerta.Visibility = Visibility.Visible;
            }

        }

        private void RestablecerPassPage_BackRequested(object sender, BackRequestedEventArgs e)
        {
            Frame root = Window.Current.Content as Frame;

            if (root.CanGoBack && !e.Handled)
            {
                e.Handled = true;
                root.GoBack();
            }
        }
    }
}
